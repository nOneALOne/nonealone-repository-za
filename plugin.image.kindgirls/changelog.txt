[B]Version 1.0.6 - 2017-06-09[/B]
- localize descriptions
- add screenshots
- fixed save pictures
[B]Version 1.0.5 - 2017-04-02[/B]
- rework picture display with thumbs
[B]Version 1.0.4 - 2016-04-21[/B]
- search function as on web site
- code into libs
[B]Version 1.0.3 - 2016-04-09[/B]
- actual photo show main page
- add Video clips archive
- save clips via context menu
[B]Version 1.0.2 - 2016-04-02[/B]
- save pictures via context menu
[B]Version 1.0.1 - 2016-04-01[/B]
- better LOG
- clean code
[B]Version 1.0.0 - 2016-03-22[/B]
- initial release
