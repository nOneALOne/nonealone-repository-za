#  Addon Kindgirls
#
#      Copyright (C) 2016
#
#  This Program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2, or (at your option)
#  any later version.
#
#  This Program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this Program; see the file LICENSE.txt.  If not, write to
#  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#  http://www.gnu.org/copyleft/gpl.html
#

import os
import sys
import urllib
import urllib2
import urlparse
import re

import buggalo

import xbmc
import xbmcgui
import xbmcaddon
import xbmcplugin

#urls to parse
BASE_URL = 'http://www.kindgirls.com'

ARCHIVE_URL = 'http://www.kindgirls.com/photo-archive/'
GIRLS_URL = 'http://www.kindgirls.com/girls/'
RANDOM = 'http://www.kindgirls.com/'
VIDEO = 'http://www.kindgirls.com/video-archive/'
SEARCH = 'http://www.kindgirls.com/search.php?s='

#see MyPics.xml
CommonRootView = 50
FullWidthList = 51
ThumbnailView = 500
PictureWrapView = 510
PictureThumbView = 514

class KindgirlsAddon(object):

#### site navigation ####

    def showSelector(self):
    
        my_log.logText('- main selector -')
                   
        my_items.addFolderItem(ADDON.getLocalizedString(30100), PATH + '?categories=actual')
        my_items.addFolderItem(ADDON.getLocalizedString(30103), PATH + '?random=show')
        my_items.addFolderItem(ADDON.getLocalizedString(30101), PATH + '?archives=show')
        my_items.addFolderItem(ADDON.getLocalizedString(30102), PATH + '?girls=select')
        my_items.addFolderItem(ADDON.getLocalizedString(30104), PATH + '?video=' + VIDEO)
        my_items.addFolderItem(ADDON.getLocalizedString(30105), PATH + '?search=show')

        xbmc.executebuiltin('Container.SetViewMode(%d)' % CommonRootView)
        xbmcplugin.endOfDirectory(HANDLE)
        
    def showGirlsSelector(self, selection):
    
        my_log.logText('- girls selector - ' + selection) 

        if(selection == 'select'):
            
            my_items.addFolderItem(ADDON.getLocalizedString(30110), PATH + '?girls=initial')
            my_items.addFolderItem(ADDON.getLocalizedString(30111), PATH + '?girls=country')
            
            xbmc.executebuiltin('Container.SetViewMode(%d)' % CommonRootView)
            xbmcplugin.endOfDirectory(HANDLE)     

        if(selection == 'initial'):

            html = download.getpage(GIRLS_URL)
              
            for m in re.finditer('value="(?P<select>[a-z])">(?P<title>[^<]*)<', html):
                   
                title = m.group('title').decode('iso-8859-1')
                link =  m.group('select') 
               
                if(link != '0'):
                    my_log.logInfo1(title, link)           
                    my_items.addFolderItem(title,  PATH + '?models=parI' + link)       
       
            xbmc.executebuiltin('Container.SetViewMode(%d)' % CommonRootView)
            xbmcplugin.endOfDirectory(HANDLE)
                
        if(selection == 'country'):
            
            html = download.getpage(GIRLS_URL)
              
            for m in re.finditer('value="(?P<select>[0-9]*)">(?P<title>[^<]*)<', html):
                    
                title = m.group('title').decode('iso-8859-1')
                link =  m.group('select') 
                                   
                if(link != '0'): 
                    my_log.logInfo1(title, link)           
                    my_items.addFolderItem(title,  PATH + '?models=parC' + link)       
       
            xbmc.executebuiltin('Container.SetViewMode(%d)' % CommonRootView)
            xbmcplugin.endOfDirectory(HANDLE)

    def loadGirls(self, selection):
    
        _selection = selection
        _selection = _selection.replace('parI','?i=')
        _selection = _selection.replace('parC','?c=')
        
        my_log.logText('- load girls - ' + _selection) 
            
        html = download.getpage(GIRLS_URL + _selection)
        
        for m in re.finditer('<div.class="model_list".*?href="(.*?)".*?img..src="(.*?)".*?alt="(.*?)"', html):  
                   
            title = m.group(3).decode('iso-8859-1')
            thumb = urlparse.urljoin(BASE_URL, m.group(2))
            link =  m.group(1) + "/"
              
            thumb =  urllib.unquote(thumb).decode('utf-8')  
            
            my_log.logInfo2(title, link, thumb)    
            my_items.addPictureItem(title, PATH + '?categories=' + link, thumb)       

        if(USE_THUMBS == True):   
            xbmc.executebuiltin('Container.SetViewMode(%d)' % ThumbnailView)                 
        xbmcplugin.endOfDirectory(HANDLE)
        
    def showArchive(self):

        my_log.logText('- archives - ') 
            
        html = download.getpage(ARCHIVE_URL)
            
        for m in re.finditer('value="(?P<date>[^"]*)">(?P<title>[^<]*)<', html):
                
            title = m.group('title').decode('iso-8859-1')
            id = title
            link =  m.group('date')
                 
            my_log.logInfo1(title, link)    
            my_items.addFolderItem(title, PATH + '?categories=' + link)
                
        xbmcplugin.endOfDirectory(HANDLE)
 
    def showCategories(self, categories):
    
        my_log.logText('- categories - ' + categories)
    
        loadUrl = BASE_URL
        
        if categories != 'actual':
            if(categories.startswith('/girls/')):
                loadUrl = urlparse.urljoin(BASE_URL, categories) # girls overview
            else:
                loadUrl = ARCHIVE_URL + "?s=" + categories # archives

        html = download.getpage(loadUrl)
                
        for m in re.finditer('<div.class="gal_list".*?href="(?P<url>[^"]*)".*?<img.src="(?P<img>[^"]*)".*?alt="(?P<title>[^"]*)"', html):

            title = m.group('title').decode('iso-8859-1')
            thumb = m.group('img')
            link = "http://www.kindgirls.com" + m.group('url') + "/"

            link = urllib.unquote(link).decode('utf-8') 
            link = link.replace('http://www.kindgirls.com/','')
                       
            url = '{0}?category={1}'.format(PATH, link)
                
            my_log.logInfo2(title, link, thumb)  
            my_items.addPictureItem(title, url , thumb)   

	# we use a special pattern, to supress the videos on actual photos        
        pattern = '<div.class="bloque"><a.href="(/gallery[^"]*)"><img[^>]*src="([^"]*)"[^>]*alt="([^"]*)"'
        if categories != 'actual':
            pattern = '<div.class="bloque"><a.href="([^"]*)"><img[^>]*src="([^"]*)"[^>]*alt="([^"]*)"'
            
        for m in re.finditer(pattern, html):

            title = m.group(3).decode('iso-8859-1')
            thumb = m.group(2)
            link = "http://www.kindgirls.com" + m.group(1) + "/"

            link =  urllib.unquote(link).decode('utf-8') 
            link = link.replace('http://www.kindgirls.com/','')
                       
            url = '{0}?category={1}'.format(PATH, link)
                
            my_log.logInfo2(title, link, thumb)  
            my_items.addPictureItem(title, url , thumb)   
       
        if(USE_THUMBS == True):   
            xbmc.executebuiltin('Container.SetViewMode(%d)' % ThumbnailView)            
        xbmcplugin.endOfDirectory(HANDLE)

    def showRandom(self):
    
        my_log.logText('- random -')
            
        html = download.getpage(RANDOM)
                
        data = ''
        for m1 in re.finditer('<div.id="randonl".*?<h3>', html):
            data = m1.group(0)
                           
        for m in re.finditer('<a.title="([^"]*)".href="([^"]*)"><img src="([^"]*)"', data):

            title = m.group(1).decode('iso-8859-1')
            thumb = m.group(3)
            link = "http://www.kindgirls.com" + m.group(2) + "/"

            link =  urllib.unquote(link).decode('utf-8') 
            link = link.replace('http://www.kindgirls.com/','')
                           
            url = '{0}?category={1}'.format(PATH, link)
                
            my_log.logInfo2(title, link, thumb)  
            my_items.addPictureItem(title, url , thumb)   
       
        if(USE_THUMBS == True):   
            xbmc.executebuiltin('Container.SetViewMode(%d)' % ThumbnailView)  
        xbmcplugin.endOfDirectory(HANDLE)

    def showCategory(self, category):
    
        my_log.logText('- category - ' + category) 
    
        html = download.getpage('http://www.kindgirls.com/' + category)

        for m in re.finditer('<div.class="gal_list".*?href="(?P<folder>.*?)".*?src="(?P<thumb>.*?)".*?alt="(?P<title>.*?)".*?href="(?P<picture>.*?)"', html):
         
            title = m.group('title').decode('iso-8859-1')
            thumb = m.group('thumb')
            link = m.group('picture')
           
            my_log.logInfo2(title, link, thumb)   
            my_items.addPictureItem2(title, link, thumb)   

        if(USE_THUMBS == True):
            xbmc.executebuiltin('Container.SetViewMode(%d)' % ThumbnailView)    
        xbmcplugin.endOfDirectory(HANDLE)
                         
    def showVideo(self, url):
    
        my_log.logText('- video - ' + url) 
    
        html = download.getpage(url)
            
        for m1 in re.finditer('href="([^"]*)"[^<]*Next[^<]*', html):
                my_items.addFolderItem(ADDON.getLocalizedString(30164), PATH + '?video=' + 'http://www.kindgirls.com' + m1.group(1))
        for m1 in re.finditer('href="([^"]*)"[^<]*Prev[^<]*', html):        
                my_items.addFolderItem(ADDON.getLocalizedString(30165), PATH + '?video=' + 'http://www.kindgirls.com' + m1.group(1))

        for m in re.finditer('class="video_list"><a.href="(?P<url>.*?)".*?img.*?src="(?P<img>.*?)".*?alt="(?P<txt>.*?)"', html):
         
            title = m.group('txt').decode('iso-8859-1')
            thumb = urlparse.urljoin (BASE_URL, m.group('img'))
            link =  urlparse.urljoin (BASE_URL, m.group('url') , "/")
              
            link =  urllib.unquote(link).decode('utf-8') 
            link = link.replace('http://www.kindgirls.com/','')
                                      
            url = '{0}?playvid={1}&thumb={2}&title={3}'.format(PATH, link, thumb, title)
               
            my_log.logInfo2(title, url, thumb)   
            my_items.addPictureItem(title, url , thumb)   

        if(USE_THUMBS == True):
            xbmc.executebuiltin('Container.SetViewMode(%d)' % ThumbnailView)    
        xbmcplugin.endOfDirectory(HANDLE)
        
    def playVideo(self, link, thumb, title):
        
        my_log.logText('- play video - ' + link)
    
        html = download.getpage('http://www.kindgirls.com/' + link)
    
        for m in re.finditer('source src="([^"]*).mp4"', html, re.DOTALL):
        
            video = m.group(1) + '.mp4'
   
            list_item = xbmcgui.ListItem(label=title, thumbnailImage=thumb)
            list_item.setArt({'thumb': thumb,
                              'icon': thumb,
                              'fanart': thumb}) 
            
            my_log.logInfo2(title, link, thumb)
            
            pluginName = os.path.basename(ADDON.getAddonInfo('path'))      
            savePath = str(xbmcplugin.getSetting(HANDLE, 'folder'))
            savePath = savePath.replace("\\", "/")
                       
            list_item.addContextMenuItems([(ADDON.getLocalizedString(30163), 'RunPlugin(%s?action=save&url=%s&path=%s)' % (PATH, video, savePath))])
            
            xbmcplugin.addDirectoryItem(HANDLE, video, list_item, False)
            xbmcplugin.setResolvedUrl(HANDLE, True, list_item)         
            break

        xbmcplugin.endOfDirectory(HANDLE)
        
    def search(self, mode):
        
        my_log.logText('- search - ' + mode)
        
        keyboard = xbmc.Keyboard('', 'Search')
        keyboard.doModal()
        
        if (keyboard.isConfirmed()):
            s = keyboard.getText()
            
            html = download.getpage(SEARCH + s)
            
            for m in re.finditer('<div.class="model_list".*?href="(.*?)".*?img..src="(.*?)".*?alt="(.*?)"', html):  
                   
                title = m.group(3).decode('iso-8859-1')
                thumb = urlparse.urljoin(BASE_URL, m.group(2))
                link =  m.group(1) + "/"
              
                thumb =  urllib.unquote(thumb).decode('utf-8')  
            
                my_log.logInfo2(title, link, thumb)    
                my_items.addPictureItem(title, PATH + '?categories=' + link, thumb)   
                     
            for m in re.finditer('<div.class="gal_list".*?href="(?P<url>[^"]*)".*?<img.src="(?P<img>[^"]*)".*?alt="(?P<title>[^"]*)"', html):

                title = m.group('title').decode('iso-8859-1')
                thumb = m.group('img')
                link = "http://www.kindgirls.com" + m.group('url') + "/"

                link = urllib.unquote(link).decode('utf-8') 
                link = link.replace('http://www.kindgirls.com/','')
                       
                url = '{0}?category={1}'.format(PATH, link)
                
                my_log.logInfo2(title, link, thumb)  
                my_items.addPictureItem(title, url , thumb)   
                
            if(USE_THUMBS == True):   
                xbmc.executebuiltin('Container.SetViewMode(%d)' % ThumbnailView)  
            xbmcplugin.endOfDirectory(HANDLE)
                     
#### save function ####

    def DownloaderClass(self,url,dest):
        dp = xbmcgui.DialogProgress()
        dp.create("Download","Downloading File",url)
        urllib.urlretrieve(url,dest,lambda nb, bs, fs, url=url: self._pbhook(nb,bs,fs,url,dp))
 
    def _pbhook(self, numblocks, blocksize, filesize, url=None,dp=None):
        try:
            percent = min((numblocks*blocksize*100)/filesize, 100)
            print percent
            dp.update(percent)
        except:
            percent = 100
            dp.update(percent)
        if dp.iscanceled(): 
            print "DOWNLOAD CANCELLED" # need to get this part working
            dp.close()
    
#### main entry point ####

if __name__ == '__main__':

    ADDON = xbmcaddon.Addon()
    ADDON_NAME = ADDON.getAddonInfo('name')
    PATH = sys.argv[0]
    HANDLE = int(sys.argv[1])
    PARAMS = urlparse.parse_qs(sys.argv[2][1:])

    ICON = os.path.join(ADDON.getAddonInfo('path'), 'icon.png')
    
    FPATH       = ADDON.getAddonInfo('path').decode("utf-8")
    RESOURCE  = xbmc.translatePath(os.path.join( FPATH, 'resources', 'lib' ).encode("utf-8") ).decode("utf-8")
    sys.path.append(RESOURCE) 
        
    import my_items
    import my_log
    import download
        
    DEBUG_PLUGIN = xbmcplugin.getSetting(HANDLE, 'debug') == "true"
    DEBUG_HTML = xbmcplugin.getSetting(HANDLE, 'debugHTML') == "true"
    USE_THUMBS = xbmcplugin.getSetting(HANDLE, 'use_thumb') == "true"
    
    xbmc.log('START KINDGIRLS')
 
    try:
            kgphoto = KindgirlsAddon()
            
            if PARAMS.has_key('category'):
                kgphoto.showCategory(PARAMS['category'][0])
            elif PARAMS.has_key('categories'):
                kgphoto.showCategories(PARAMS['categories'][0])
            elif PARAMS.has_key('archives'):
                kgphoto.showArchive()
            elif PARAMS.has_key('girls'):
                kgphoto.showGirlsSelector(PARAMS['girls'][0])
            elif PARAMS.has_key('models'):
                kgphoto.loadGirls(PARAMS['models'][0])
            elif PARAMS.has_key('random'):
                kgphoto.showRandom()
            elif PARAMS.has_key('video'):
                kgphoto.showVideo(PARAMS['video'][0])
            elif PARAMS.has_key('playvid'):
                kgphoto.playVideo(PARAMS['playvid'][0],PARAMS['thumb'][0],PARAMS['title'][0])
            elif PARAMS.has_key('search'):
                kgphoto.search(PARAMS['search'][0])
            elif PARAMS.has_key('action'):
                try:    
                    filename = os.path.basename(PARAMS['url'][0])
                    kgphoto.DownloaderClass(PARAMS['url'][0],PARAMS['path'][0] + "/" +  filename)
                except:
                    xbmc.log('KINDGIRLS - ERROR SAVING -') 
            else:
                kgphoto.showSelector()
    except Exception:
        buggalo.onExceptionRaised() 